/*
 Navicat Premium Data Transfer

 Source Server         : Kecamatan Margaasih
 Source Server Type    : MySQL
 Source Server Version : 50640
 Source Host           : www.kecamatanmargaasih.bandungkab.go.id:3306
 Source Schema         : margaasi_margaasih

 Target Server Type    : MySQL
 Target Server Version : 50640
 File Encoding         : 65001

 Date: 27/08/2018 03:46:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for captcha
-- ----------------------------
DROP TABLE IF EXISTS `captcha`;
CREATE TABLE `captcha`  (
  `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `word` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`captcha_id`) USING BTREE,
  INDEX `word`(`word`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 491 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of captcha
-- ----------------------------
INSERT INTO `captcha` VALUES (490, 1534757409, '4UE6P');

-- ----------------------------
-- Table structure for dlmbg_admin_dinas
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_admin_dinas`;
CREATE TABLE `dlmbg_admin_dinas`  (
  `id_admin_dinas` int(5) NOT NULL AUTO_INCREMENT,
  `nama_admin_dinas` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username_admin_dinas` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_bidang` int(5) NOT NULL,
  PRIMARY KEY (`id_admin_dinas`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_admin_dinas
-- ----------------------------
INSERT INTO `dlmbg_admin_dinas` VALUES (1, 'HANSAH DARMAWAN', 'hansah007', '2eafea412f102257febf708a322b167a', 5);

-- ----------------------------
-- Table structure for dlmbg_admin_sekolah
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_admin_sekolah`;
CREATE TABLE `dlmbg_admin_sekolah`  (
  `id_admin_sekolah` int(5) NOT NULL AUTO_INCREMENT,
  `id_sekolah` int(5) NOT NULL,
  `nama_operator` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_admin_sekolah`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_admin_sekolah
-- ----------------------------
INSERT INTO `dlmbg_admin_sekolah` VALUES (1, 4, 'Hansah Darmawan', 'hansah', '2eafea412f102257febf708a322b167a', 'hansahdarmawan@Gmail.com');

-- ----------------------------
-- Table structure for dlmbg_admin_super
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_admin_super`;
CREATE TABLE `dlmbg_admin_super`  (
  `id_admin_super` int(5) NOT NULL AUTO_INCREMENT,
  `nama_super_admin` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username_super_admin` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_admin_super`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_admin_super
-- ----------------------------
INSERT INTO `dlmbg_admin_super` VALUES (1, 'Administrator', 'admin', '2eafea412f102257febf708a322b167a');
INSERT INTO `dlmbg_admin_super` VALUES (6, 'kecmargaasih', 'kecmargaasih', 'a03db35f3f1d29d4633f2df019a96cf1');

-- ----------------------------
-- Table structure for dlmbg_counter
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_counter`;
CREATE TABLE `dlmbg_counter`  (
  `id_counter` int(10) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_counter`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3447 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_counter
-- ----------------------------
INSERT INTO `dlmbg_counter` VALUES (3383, '180.245.241.67', '09-Aug-2018 17:14:02');
INSERT INTO `dlmbg_counter` VALUES (3384, '180.245.241.67', '09-Aug-2018 18:21:51');
INSERT INTO `dlmbg_counter` VALUES (3385, '180.245.241.67', '09-Aug-2018 18:22:04');
INSERT INTO `dlmbg_counter` VALUES (3386, '220.247.175.58', '10-Aug-2018 11:15:55');
INSERT INTO `dlmbg_counter` VALUES (3387, '220.247.175.58', '10-Aug-2018 11:16:25');
INSERT INTO `dlmbg_counter` VALUES (3388, '34.246.182.110', '10-Aug-2018 14:04:53');
INSERT INTO `dlmbg_counter` VALUES (3389, '180.253.218.230', '11-Aug-2018 23:29:01');
INSERT INTO `dlmbg_counter` VALUES (3390, '125.161.33.127', '13-Aug-2018 10:32:39');
INSERT INTO `dlmbg_counter` VALUES (3391, '125.161.33.127', '13-Aug-2018 10:33:08');
INSERT INTO `dlmbg_counter` VALUES (3392, '140.213.25.12', '13-Aug-2018 16:25:05');
INSERT INTO `dlmbg_counter` VALUES (3393, '36.71.235.40', '13-Aug-2018 16:28:44');
INSERT INTO `dlmbg_counter` VALUES (3394, '66.102.6.82', '13-Aug-2018 16:45:19');
INSERT INTO `dlmbg_counter` VALUES (3395, '66.102.6.82', '13-Aug-2018 16:45:23');
INSERT INTO `dlmbg_counter` VALUES (3396, '220.247.175.58', '13-Aug-2018 16:52:41');
INSERT INTO `dlmbg_counter` VALUES (3397, '37.9.113.183', '14-Aug-2018 06:53:21');
INSERT INTO `dlmbg_counter` VALUES (3398, '36.71.234.122', '14-Aug-2018 10:33:38');
INSERT INTO `dlmbg_counter` VALUES (3399, '36.71.234.122', '14-Aug-2018 13:42:24');
INSERT INTO `dlmbg_counter` VALUES (3400, '36.71.234.122', '14-Aug-2018 13:42:51');
INSERT INTO `dlmbg_counter` VALUES (3401, '36.71.234.122', '14-Aug-2018 13:44:03');
INSERT INTO `dlmbg_counter` VALUES (3402, '40.77.167.58', '15-Aug-2018 06:16:30');
INSERT INTO `dlmbg_counter` VALUES (3403, '114.124.212.19', '15-Aug-2018 07:59:29');
INSERT INTO `dlmbg_counter` VALUES (3404, '114.124.212.19', '15-Aug-2018 08:04:37');
INSERT INTO `dlmbg_counter` VALUES (3405, '180.245.188.148', '15-Aug-2018 13:02:25');
INSERT INTO `dlmbg_counter` VALUES (3406, '180.245.188.148', '15-Aug-2018 13:02:34');
INSERT INTO `dlmbg_counter` VALUES (3407, '110.136.155.176', '15-Aug-2018 13:42:43');
INSERT INTO `dlmbg_counter` VALUES (3408, '220.247.175.58', '15-Aug-2018 13:49:52');
INSERT INTO `dlmbg_counter` VALUES (3409, '220.247.175.58', '15-Aug-2018 14:23:58');
INSERT INTO `dlmbg_counter` VALUES (3410, '110.136.155.176', '15-Aug-2018 14:25:59');
INSERT INTO `dlmbg_counter` VALUES (3411, '114.124.215.234', '16-Aug-2018 00:46:42');
INSERT INTO `dlmbg_counter` VALUES (3412, '114.124.215.234', '16-Aug-2018 00:47:19');
INSERT INTO `dlmbg_counter` VALUES (3413, '114.124.245.123', '16-Aug-2018 00:47:20');
INSERT INTO `dlmbg_counter` VALUES (3414, '220.247.175.58', '16-Aug-2018 14:36:35');
INSERT INTO `dlmbg_counter` VALUES (3415, '180.245.245.186', '16-Aug-2018 17:47:38');
INSERT INTO `dlmbg_counter` VALUES (3416, '180.253.6.145', '17-Aug-2018 12:41:39');
INSERT INTO `dlmbg_counter` VALUES (3417, '180.253.6.145', '17-Aug-2018 12:42:04');
INSERT INTO `dlmbg_counter` VALUES (3418, '180.253.6.145', '17-Aug-2018 20:02:12');
INSERT INTO `dlmbg_counter` VALUES (3419, '180.253.6.145', '17-Aug-2018 20:02:36');
INSERT INTO `dlmbg_counter` VALUES (3420, '180.253.6.145', '17-Aug-2018 20:03:19');
INSERT INTO `dlmbg_counter` VALUES (3421, '180.253.6.145', '17-Aug-2018 20:03:49');
INSERT INTO `dlmbg_counter` VALUES (3422, '180.253.6.145', '17-Aug-2018 20:28:19');
INSERT INTO `dlmbg_counter` VALUES (3423, '180.253.6.145', '18-Aug-2018 10:48:25');
INSERT INTO `dlmbg_counter` VALUES (3424, '180.253.6.145', '18-Aug-2018 10:48:52');
INSERT INTO `dlmbg_counter` VALUES (3425, '180.253.6.145', '19-Aug-2018 09:52:32');
INSERT INTO `dlmbg_counter` VALUES (3426, '180.253.6.145', '19-Aug-2018 09:52:58');
INSERT INTO `dlmbg_counter` VALUES (3427, '114.124.214.37', '20-Aug-2018 01:25:54');
INSERT INTO `dlmbg_counter` VALUES (3428, '112.215.208.125', '20-Aug-2018 16:32:36');
INSERT INTO `dlmbg_counter` VALUES (3429, '112.215.208.125', '20-Aug-2018 16:36:07');
INSERT INTO `dlmbg_counter` VALUES (3430, '112.215.208.125', '20-Aug-2018 16:38:46');
INSERT INTO `dlmbg_counter` VALUES (3431, '180.245.165.151', '20-Aug-2018 18:19:07');
INSERT INTO `dlmbg_counter` VALUES (3432, '180.245.165.151', '20-Aug-2018 18:21:08');
INSERT INTO `dlmbg_counter` VALUES (3433, '180.253.6.145', '21-Aug-2018 12:12:11');
INSERT INTO `dlmbg_counter` VALUES (3434, '180.253.6.145', '21-Aug-2018 12:12:39');
INSERT INTO `dlmbg_counter` VALUES (3435, '178.154.200.66', '21-Aug-2018 17:08:14');
INSERT INTO `dlmbg_counter` VALUES (3436, '180.253.20.250', '23-Aug-2018 17:30:25');
INSERT INTO `dlmbg_counter` VALUES (3437, '180.253.20.250', '23-Aug-2018 17:30:52');
INSERT INTO `dlmbg_counter` VALUES (3438, '180.253.20.250', '23-Aug-2018 17:35:57');
INSERT INTO `dlmbg_counter` VALUES (3439, '220.247.175.58', '24-Aug-2018 13:22:34');
INSERT INTO `dlmbg_counter` VALUES (3440, '220.247.175.58', '24-Aug-2018 13:22:37');
INSERT INTO `dlmbg_counter` VALUES (3441, '8.37.235.165', '25-Aug-2018 09:30:17');
INSERT INTO `dlmbg_counter` VALUES (3442, '36.71.242.89', '25-Aug-2018 09:30:33');
INSERT INTO `dlmbg_counter` VALUES (3443, '36.72.29.169', '25-Aug-2018 18:54:44');
INSERT INTO `dlmbg_counter` VALUES (3444, '180.253.20.250', '26-Aug-2018 13:51:06');
INSERT INTO `dlmbg_counter` VALUES (3445, '36.72.29.169', '26-Aug-2018 16:05:42');
INSERT INTO `dlmbg_counter` VALUES (3446, '180.253.20.250', '26-Aug-2018 20:11:24');

-- ----------------------------
-- Table structure for dlmbg_dinas_download
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_dinas_download`;
CREATE TABLE `dlmbg_dinas_download`  (
  `id_dinas_download` int(10) NOT NULL AUTO_INCREMENT,
  `judul_file` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_file` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_admin_dinas` int(5) NOT NULL,
  `id_bidang` int(5) NOT NULL,
  `stts` int(1) NOT NULL,
  PRIMARY KEY (`id_dinas_download`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for dlmbg_menu
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_menu`;
CREATE TABLE `dlmbg_menu`  (
  `id_menu` int(5) NOT NULL AUTO_INCREMENT,
  `id_parent` int(5) NOT NULL,
  `menu` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `url_route` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `content` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `posisi` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_menu`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_menu
-- ----------------------------
INSERT INTO `dlmbg_menu` VALUES (1, 0, 'Profil ', '', '<p>Katapang adalah sebuah kecamatan di Kabupaten Bandung, Provinsi Jawa Barat, Indonesia. Kecamatan Katapang merupakan wilayah penyangga terpenting bagi Soreang sebagai ibukota Kabupaten Bandung, dan merupakan garda terdepan dalam hal pembangunan industri di kawasan Soreang dan sekitarnya, mengingat Katapang merupakan salah satu kawasan berikat industri di Bandung Selatan.</p><p>Wilayah administrasi Kecamatan Katapang terdiri dari :</p><p><span  initial;\">7 (Tujuh) Desa :</span></p><p>1. Banyusari</p><p>2. Cilampeni</p><p>3. Gandasari</p><p>4. Katapang</p><p>5. Pangauban</p><p>6. Sangkanhurip</p><p>7. Sukamukti</p>\n', 'atas');
INSERT INTO `dlmbg_menu` VALUES (2, 0, 'Sekretariat', '', '', 'atas');
INSERT INTO `dlmbg_menu` VALUES (3, 0, 'Desa', '', '', 'atas');
INSERT INTO `dlmbg_menu` VALUES (4, 0, 'Galeri', '/web/galeri', '', 'atas');
INSERT INTO `dlmbg_menu` VALUES (6, 1, 'Sambutan', '', '<p align=\"justipy\" center;\"=\"\"  center;\"><b  &nbsp;<span  rgb(0, 0, 0);\">SELAMAT DATANG</span></b></p>\n\n<p justify;\"=\"\"  justify;\">Berkaitan dengan kebutuhan akan informasi seputar Kegiatan pelayanan yang bersifat primer maupun sekunder diselenggarakan pemerintah sebagai bentuk perwujudan loyalitasnya sebagai abdi masyarakat, kegiatan tersebut dilaksanakan secara efisien dan responsive yaitu pelayanan dilaksanakan melalui pemanfaatan sumberdaya yang seminimal mungkin dengan hasil yang maksimal dan berdasar pada kebutuhan yang diharapkan oleh masyarakat.&nbsp; Di bidang Pemerintahan tidaklah kalah pentingnya pelayanan itu, bahkan perannya lebih besar karena menyangkut kepentingan umum, bahkan kepentingan rakyat secara keseluruhan. Karena peran pelayanan umum yang diselenggarakan oleh pemerintah melibatkan seluruh aparat Pegawai Negeri makin terasa dengan adanya peningkatan kesadaran bernegara dan bermasyarakat, maka pelayanan telah meningkat kedudukannya di mata masyarakat menjadi suatu hak, yaitu hak atas pelayanan di kecamatan katapang. Namun ternyata hak masyarakat atau perorangan untuk memperoleh pelayanan dari aparat pemerintah terasa belum dapat memenuhi harapan semua pihak, baik masyarakat itu sendiri maupun Pemerintah dan pelayanan umum belum menjadi “budaya” masyarakat.</p>\n', 'atas');
INSERT INTO `dlmbg_menu` VALUES (7, 1, 'Visi dan Misi', '', '<blockquote 0=\"\" 40px;=\"\" border:=\"\" none;=\"\" padding:=\"\" 0px;\"=\"\">\n<p center;\"=\"\"><img src=\"https://lh6.googleusercontent.com/2rXCXePT2xMcFNeBdtFvd28gxLKK1O4o7ptwJOaoyVBgqCKn8VHzm9XBeEzUP56-TEhSKaKdNuXfPbT-Lijm=w1600-h777\" p=\"\"  default;\"></p></blockquote><blockquote 0=\"\" 40px;=\"\" border:=\"\" none;=\"\" padding:=\"\" 0px;\"=\"\"><blockquote 0=\"\" 40px;=\"\" border:=\"\" none;=\"\" padding:=\"\" 0px;\"=\"\"><blockquote 0=\"\" 40px;=\"\" border:=\"\" none;=\"\" padding:=\"\" 0px;\"=\"\"><h1 center;\"=\"\"><span normal;\"=\"\">&nbsp; &nbsp; &nbsp; VISI</span></h1></blockquote></blockquote></blockquote><blockquote 0=\"\" 40px;=\"\" border:=\"\" none;=\"\" padding:=\"\" 0px;\"=\"\"><h1 center;\"=\"\"><p></p></h1>\n</blockquote>\n<h4 center;\"=\"\"  center;\"><span normal;\"=\"\">Memantapkan Kabupaten Bandung yang Maju, Mandiri, dan Berdaya Saing, melalui Tata Kelola Pemerintahan yang Baik dan Sinergi Pembangunan Perdesaan, Berlandaskan Religius, Kultural dan Berwawasan Lingkungan</span></h4>\n<blockquote 0=\"\" 40px;=\"\" border:=\"\" none;=\"\" padding:=\"\" 0px;\"=\"\"><blockquote 0=\"\" 40px;=\"\" border:=\"\" none;=\"\" padding:=\"\" 0px;\"=\"\"><blockquote 0=\"\" 40px;=\"\" border:=\"\" none;=\"\" padding:=\"\" 0px;\"=\"\"><blockquote 0=\"\" 40px;=\"\" border:=\"\" none;=\"\" padding:=\"\" 0px;\"=\"\"><h1 center;\"=\"\"><span normal;\"=\"\">MIsI</span></h1></blockquote></blockquote></blockquote></blockquote>\n\n<p><span normal;\"=\"\"></span></p>\n\n<ol>\n <li><h3 left;\"=\"\">Peningkatan kualitas SDM</h3>\n</li>\n <li><h3 left;\"=\"\">Menciptakan pembangunan ekonomi yang berdaya saing</h3>\n</li>\n <li><h3 left;\"=\"\">Mewujudkan pembangunan infrastruktur dasar terpadu tata ruang wilayah</h3>\n</li>\n <li><h3 left;\"=\"\">&nbsp;Meningkatkan Kualitas lingkungan hidup</h3>\n</li>\n <li><h3 left;\"=\"\">&nbsp;Mewujudkan tata kelola pemerintah yang baik dan bersih</h3>\n</li>\n</ol>\n', 'atas');
INSERT INTO `dlmbg_menu` VALUES (8, 2, 'Struktur Kepegawaian', '', '<h6>STRUKTUR ORGANISASI KECAMATAN MARGAHAYU</h6><p>Adapun tugas dari masing masing tokoh adalah:</p><h6>Camat</h6><ul><li>Perumusan, pengaturan, pengkoordinasian, pembinaan dan pelaksanaan kebijakan umum dan teknis operasional bidang pemerintahan, pemberdayaan masyarakat, ketentraman dan ketertiban umum, pembangunan dan sosial budaya.</li><li>Penyelenggaraan pengendalian dan fasilitasi&nbsp; pelaksanaan tugas bidang pemerintahan, pemberdayaan masyarakat, ketentraman dan ketertiban umum, pembangunan dan sosial budaya.</li><li>Penyelenggaraan koordinasi, integrasi dan sinkronisasi sesuai dengan lingkup tugasnya; dan penyelenggaraan monitoring, evaluasi dan pelaporan capaian kinerja Kecamatan.</li></ul><h6>Sekretaris</h6><ul><li>Penyusunan rencana kerja</li><li>Pengumpulan, pengolahan usulan program dan kegiatan.</li><li>Penyelenggaraan tugas-tugas kesekretariatan.</li><li>Penyelenggaraan pengendalian pelaksanaan kegiatan pelayanan umum dan kepegawaian, keuangan serta perencanaan, evaluasi dan pelaporan.</li><li>Penyelenggaraan dan pengkoordinasian Pelayanan Administrasi Terpadu Kecamatan (PATEN).</li><li>Penyelenggaraan koordinasi, integrasi dan sinkronisasi sesuai dengan lingkup tugasnya.</li><li>Penyelenggaraan monitoring, evaluasi dan pelaporan capaian kinerja sekretariat.</li></ul><h6>Kasubag Program dan Keuangan</h6><ul><li>Penyusunan bahan perencanaan dan pelayanan administrasi perencanaan dan bahan rencana anggaran Kecamatan.</li><li>Pelaksanaan teknis administrasi pengelolaan keuangan</li><li>Pelaksanaan penyusunan laporan keuangan Kecamatan.</li><li>Pelaksanaan koordinasi, integrasi dan sinkronisasi sesuai dengan lingkup tugasnya.</li><li>Pelaksanaan monitoring, evaluasi dan pelaporan capaian kinerja Subbagian Program dan Keuangan.</li></ul><h6>Kasubag Umum dan Kepegawaian</h6><ul><li>Penyusunan bahan pelaksanaan pelayanan umum dan kepegawaian, kelembagaan serta ketatalaksanaan.</li><li>Pelaksanaan urusan surat menyurat, kearsipan, perpustakaan, kehumasan, keprotokolan, barang milik daerah/aset, rumah tangga kedinasan dan administrasi kepegawaian.</li><li>Pelaksanaan koordinasi, integrasi dan sinkronisasi sesuai dengan lingkup tugasnya.</li><li>Pelaksanaan monitoring, evaluasi dan pelaporan capaian kinerja Subbagian Umum dan Kepegawaian.</li><li>Pelaksanaan evaluasi dan pelaporan pelaksanaan tugas.</li><li>Pelaksanaan tugas kedinasan lain sesuai dengan bidang tugas&nbsp; dan fungsinya.</li></ul><h6>Seksi Pemerintahan</h6><ul><li>Penyiapan bahan perumusan kebijakan teknis operasional Seksi Pemerintahan.</li><li>Penyusunan dan pelaksanaan rencana kerja Seksi Pemerintahan.</li><li>Pengumpulan dan Pengolahan data Seksi Pemerintahan.</li><li>Pelaksanaan koordinasi, integrasi dan sinkronisasi sesuai dengan lingkup tugasnya.</li><li>Pelaksanaan monitoring, evaluasi dan pelaporan capaian kinerja Seksi Pemerintahan.</li></ul><h6>Seksi Pemberdayaan Masyarakat</h6><ul><li>Penyiapan bahan perumusan kebijakan teknis operasional Seksi Pemberdayaan Masyarakat.</li><li>Penyusunan dan pelaksanaan rencana kerja Seksi Pemberdayaan Masyarakat.</li><li>Pengumpulan dan Pengolahan data Seksi Pemberdayaan Masyarakat.</li><li>Pelaksanaan koordinasi, integrasi dan sinkronisasi sesuai dengan tugasnya.</li><li>Pelaksanaan monitoring, evaluasi dan pelaporan capaian kinerja Seksi Pemberdayaan Masyarakat.</li></ul><h6>Seksi Pembangunan</h6><ul><li>Penyiapan bahan perumusan kebijakan teknis operasional Seksi Pembangunan.</li><li>Penyusunan dan pelaksanaan rencana kerja Seksi Pembangunan.</li><li>Pengumpulan dan Pengolahan data Seksi Pembangunan.</li><li>Pelaksanaan koordinasi, integrasi dan sinkronisasi sesuai dengan tugasnya.</li><li>Pelaksanaan monitoring, evaluasi dan pelaporan capaian kinerja Seksi Pembangunan.</li></ul><h6>Seksi Sosial Budaya</h6><ul><li>Penyiapan bahan perumusan kebijakan teknis operasional Seksi Sosial Budaya.</li><li>Penyusunan dan pelaksanaan rencana kerja Sosial Budaya.</li><li>Pengumpulan dan Pengolahan data Seksi Sosial Budaya.</li><li>Pelaksanaan koordinasi, integrasi dan sinkronisasi sesuai dengan bidang tugasnya.</li><li>Pelaksanaan monitoring, evaluasi dan pelaporan capaian kinerja Seksi Sosial Budaya.</li></ul><h6>Seksi Ketentraman dan Ketertiban Umum</h6><ul><li>Penyiapan bahan perumusan kebijakan teknis operasional Seksi Ketentraman dan Ketertiban Umum.</li><li>Penyusunan dan pelaksanaan rencana kerja Seksi Ketentraman dan Ketertiban Umum</li><li>Pengumpulan dan Pengolahan data Seksi Ketenteraman dan Ketertiban Umum.</li><li>Pelaksanaan koordinasi, integrasi dan sinkronisasi sesuai dengan tugasnya.</li><li>Pelaksanaan tugas tambahan ex-officio Kepala Seksi Ketenteraman dan Ketertiban Umum pada Kecamatan sebagai Kepala Satuan Unit Pelaksana Polisi Pamong Praja.</li><li>Pelaksanaan monitoring, evaluasi dan pelaporan capaian kinerja Seksi Ketenteraman dan Ketertiban Umum.</li></ul><p></p>\n', 'atas');
INSERT INTO `dlmbg_menu` VALUES (9, 2, 'Data Kepegawaian', '', '<b>\n<p center;\"=\"\"></p>\n</b><center><b><b>APARATUR KANTOR KECAMATAN PAMEUNGPEUK KABUPATEN BANDUNG&nbsp;</b><b>TAHUN 2018</b></b><center><b><br>\n</b>\n<table rules=\"all\">\n<tbody class=\"redactor-current-td\">\n <tr class=\"redactor-current-td\">\n  <td class=\"\" center;\"=\"\"><b>NO</b></td>\n\n  <td center;\"=\"\" class=\"\"><b class=\"redactor-current-td\"><center>NAMA/NIP</center></b></td>\n\n  <td center;\"=\"\" class=\"\"><b class=\"redactor-current-td\"><center>JABATAN</center></b></td>\n </tr>\n\n <tr>\n  <td hight=\"100px\">1</td>\n\n  <td width=\"200px\" class=\"\">MOCHAMAD USMAN,S.Sos,M.Si<br>NIP.&nbsp;197110091991011001</td>\n\n  <td>Camat</td>\n </tr>\n\n <tr>\n  <td>2</td>\n\n  <td class=\"\">Beni<br>\n NIP. 19830117 200112 1 002</td>\n\n  <td>Sekretaris Kecamatan</td>\n </tr>\n\n <tr>\n  <td>3</td>\n\n  <td class=\"\">Lilis<br>\n NIP. 19611103 198702 2 002</td>\n\n  <td>Kasie. Pemberdayaan Masyarakat</td>\n </tr>\n\n <tr>\n  <td>4</td>\n\n  <td class=\"\">NANANG SUPRIATNA<br>NIP. 196604231991031005</td>\n\n  <td class=\"\">Kasie. Pemerintahan</td>\n </tr>\n\n <tr>\n  <td>5</td>\n\n  <td class=\"\">DODI<br>\n NIP. 19620111 198603 1 004</td>\n\n  <td class=\"\">Kasie. Trantibum</td>\n </tr>\n\n <tr>\n  <td>6</td>\n\n  <td class=\"\">TETI NURYATI<br>\n NIP. 196612251996032001</td>\n\n  <td>Kasie. Sosial &amp; Budaya</td>\n </tr>\n\n <tr>\n  <td>7</td>\n\n  <td class=\"\">SUKIS MAWARDI<br>\n NIP.&nbsp;196010041996011001</td>\n\n  <td class=\"\">Kasie. Pembangunan</td>\n </tr>\n\n <tr>\n  <td>8</td>\n\n  <td class=\"\">YULIE ROSYDAH KOMAR<br>\n NIP. 197110091998032001</td>\n\n  <td class=\"\">Ka. Sub. Bag. Keuangan &amp; Program</td>\n </tr>\n\n <tr>\n  <td>9</td>\n\n  <td class=\"\">Hj. YUNI ANGGRIANI<br>\n NIP.&nbsp;197106071995032001</td>\n\n  <td>Ka. Su. Bag. Umum &amp; Kepegawaian</td>\n </tr>\n\n <tr>\n  <td>10</td>\n\n  <td class=\"\">IDA HADIANTI<br>\n NIP. 19610807 198303 1 013</td>\n\n  <td>Administrasi Umum Sosial Budaya</td>\n </tr>\n\n <tr>\n  <td class=\"\">11</td><td class=\"\">KURNIATI<br>NIP.&nbsp;197009082007012008</td><td class=\"redactor-current-td\">Penerima ahli waris&nbsp;</td></tr>\n</tbody>\n</table><br></center><center><br></center><center left;\"=\"\">Sumber : Arsip Kecamatan Margahayu Kabupaten Bandung&nbsp;</center></center>\n', 'atas');
INSERT INTO `dlmbg_menu` VALUES (13, 0, 'Beranda', '/web/web', '', 'bawah');
INSERT INTO `dlmbg_menu` VALUES (14, 0, 'Berita', '/web/berita', '', 'bawah');
INSERT INTO `dlmbg_menu` VALUES (15, 0, 'Pengumuman', '/web/pengumuman', '', 'bawah');
INSERT INTO `dlmbg_menu` VALUES (16, 0, 'Agenda Dinas', '/web/agenda', '', 'bawah');
INSERT INTO `dlmbg_menu` VALUES (17, 0, 'List Download', '', '<p></p><ol><li><a href=\"http://www.bandungkab.go.id/public/uploads/20170810103052-monografi-kecamatan-pameungpeuk-tahun-2016.pdf\" target=\"\">Monografi Kecamatan Pameungpeuk Kabupaten Bandung</a>&nbsp;</li><li><a href=\"http://www.bandungkab.go.id/public/uploads/20170810013135-mekanisme-pelayanan-administrasi-terpadu.pdf\" target=\"\">Mekanisme Pelayanan Administrasi Terpadu</a>&nbsp;</li><li><a href=\"http://www.bandungkab.go.id/public/uploads/20170814111906-perda-no-5-2015.pdf\"px=\"\" none=\"\">Peraturan Daerah Kabupaten Bandung Nomor 5 Tahun 2015 Tentang Penyelenggaraan Ketenteraman, Ketertiban Umum, Dan Pelindungan Masyarakat</a></li><li><a href=\"https://rukuntetangga3.files.wordpress.com/2010/07/contoh-surat-keterangan.doc\" target=\"\">Contoh Surat Keterangan</a>&nbsp;</li></ol><p></p>\n', 'bawah');
INSERT INTO `dlmbg_menu` VALUES (18, 0, 'Buku Tamu', '/web/buku_tamu', '', 'bawah');
INSERT INTO `dlmbg_menu` VALUES (19, 3, 'Pemerintahan Desa', '/web/data_desa', '<center><br></center>\n', 'atas');
INSERT INTO `dlmbg_menu` VALUES (28, 1, 'Sejarah Kecamatan', '', '<p center;\"=\"\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b> &nbsp; &nbsp; SEJARAH MARGAHAYU</b></p><p center;\"=\"\"><img src=\"https://lh5.googleusercontent.com/qwgfh3Ky-SGhST-_vUN5xgmb7133-8ZDhPdE2RQg4mzIiDPMY1FjVxo-xOWFcntq9baAG_cP6pJUk9tuRwSZ=w1600-h728\"  nw-resize; width: 16px; height: 16px;\"></p><p center;\"=\"\">Kecamatan Margahayu dibentuk hasil pemekaran kecamatan Dayeuhkolot sebagai&nbsp;</p><p center;\"=\"\">kecamatan<span initial;\"=\"\">&nbsp;induknya pada tahun 1989, hal ini bersamaan&nbsp;&nbsp;</span><span initial;\"=\"\">dengan penataan&nbsp;</span></p><p center;\"=\"\"><span initial;\"=\"\">batas Kota&nbsp;</span><span initial;\"=\"\">Bandung dengan&nbsp;</span><span initial;\"=\"\">&nbsp;Kabupaten Bandung (1987) dimana terjadi&nbsp;</span></p><p center;\"=\"\"><span initial;\"=\"\">perluasan wilayah&nbsp;&nbsp;</span><span initial;\"=\"\">Kota Bandung.</span></p><pre>Batas-batas Kecamatan Margahayu :\nSebelah Timur   : Kecamatan Dayeuhkolot\nSebelah Barat   : Kecamatan Margaasih\nSebelah Utara   : Kota Bandung \nSebelah Selatan : Kecamatan Katapang\nAwal pembentukan letak kantor Kecamatan Margahayu bertempat di \nkomplek Taman Kopo Indah Blok F, \nkemudian seiring kebijakan \notonomi daerah (UU 22/1999) dengan adanya \npenghapusan Kewedanaan (dipimpin seorang Wadana-pembantu bupati),\nmaka pada tahun 2000 kantornya \nberpindah ke eks-kantor \nWadana Margahayu di jalan Sukamenak No. 145.\n\n<pre>Wilayah administrasi Kecamatan Margahayu terdiri dari 1\nKelurahan dan 4 Desa :\n1. Desa Sukamenak\n2. Desa Sayati\n3. Desa Margahayu Selatan\n4. Desa Margahayu Tengah\n5. Kelurahan Sulaiman</pre></pre>\n', 'atas');
INSERT INTO `dlmbg_menu` VALUES (29, 1, 'Potensi UMKM', '', '<div  justify;\">Bukan hal yang asing di telinga bahwa sektor usaha mikro kecil menengah ( UMKM) merupakan salah satu motor penggerak perekonomian Indonesia dan menjadi fokus pemerintahan. Jumlah pelaku UMKM di Indonesia dilaporkan mencapai 49 juta dan diprediksi menyerap lebih dari 107 juta tenaga kerja. Kontribusi sektor UMKM terhadap produk domestik bruto (PDB) pun semakin meningkat dalam lima tahun terakhir di mana Kementerian Koperasi dan Usaha Kecil Menengah mencatat lonjakan dari 57,84 persen menjadi 60,34 persen di tahun 2016. Di antara UMKM, industri ekonomi kreatif juga tercatat berkembang positif dengan pertumbuhan 5,6 persen antara 2010-2013. Industri ini menyumbang 7,1 persen terhadap PDB dan berhasil menyerap sekitar 12 juta tenaga kerja, menjadikannya salah satu ranah andalan untuk mendorong peningkatan pendapatan masyarakat serta berperan strategis dalam memerangi pengangguran dan kemiskinan Potensi UMKM yang ada di wilayah Kecamatan Margahayu Kabupaten Bandung Khususnya banyak berpotensi dan menghasilkan kreasi kreatif yang menunjang perekonomian warga masyarakat sekitar wiayah Kecamatan Margahayu Kabupaten Bandung yang diantaranya :</div>', 'atas');
INSERT INTO `dlmbg_menu` VALUES (30, 2, 'Rencana Kerja', '', '<p>Rencana Kerja Perubahan Kecamatan Pameungpeuk Kabupaten Bandung tahun 2018, merupakan pedoman dan rujukan dalam menyusun program dan kegiatan Kecamatan Pameungpeuk Kabupaten Bandung tahun 2018 yang telah ditetapkan dalam Rencana Kerja Perubahan Pemerintah Daerah, yang mengarah pada pencapaian sasaran-sasaran pembangunan yang dalam penyusunannya juga memperhatikan program dan kebijakan dari Pemerintah Pusat yang dilaksanakan di daerah.</p>\n\n<p>Diharapkan Renja ini dapat dijadikan sarana peningkatan kinerja Kecamatan Pameungpeuk Kabupaten Bandung dan juga dapat memberikan umpan balik yang sangat diperlukan dalam pengambilan keputusan dan penyusunan rencana kerja di masa mendatang oleh seluruh pegawai Kecamatan Pameungpeuk Kabupaten Bandung sehingga akan diperoleh peningkatan kinerja ke arah yang lebih baik dimasa mendatang.&nbsp;</p>\n\n<p><a href=\"http://www.bandungkab.go.id/uploads/20180605021612-renja-2018-kecamatan-pameungpeuk.pdf\"><img src=\"https://4.bp.blogspot.com/-D2cqy2tvRxY/VMcXnsxvF_I/AAAAAAAAE8c/yQHeGDxeVsA/s1600/download+free+movie+qubool+hai.gif\" width=\"200\" height=\"50\" title=\"Rencana Kerja Kecamatan Pameungpeuk Tahun 2018\" alt=\"Kec.Pameungpeuk\" border=\"none\"  default;\"></a></p>\n\n\n<p>Sumber : Bidang Program dan Keuangan Kecamatan Pameungpeuk Kab. Bandung</p>\n\n<p><b><br>\n</b></p>', 'atas');
INSERT INTO `dlmbg_menu` VALUES (31, 2, 'LKIP', '', '<p>Dengan mengucap puji syukur kehadirat Allah SWT, atas rahmat dan karunia-Nya, Laporan Kinerja&nbsp; Instansi Pemerintah (LKIP) Kecamatan Pameungpeuk Kabupaten Bandung Tahun 2017 dapat diselesaikan.</p>\n\n<p>Penyusunan LKIP merupakan kewajiban sebagaimana diamanatkan dalam Instruksi Presuden Nomor 7 Tahun 1999, untuk mempertanggungjawabkan pelaksanaan tugas pokok dan fungsinya yang dipercayakan kepada Kecamatan Pameungpeuk dalam melayani masyarakat berdasarkan Rencana Strategis (Renstra) kecamatan pameungpeuk Tahun 2016-2021 </p>\n\n<p>Dengan tersusunnya LKIP Kecamatan Pameungpeuk tahun 2017 ini, kami menyampaikan terimakasih kepada berbagai pihak yang telah membantu&nbsp; dalam penyelesaian LKIP ini. Kami juga berterimakasih kepada pelaksana kegiatan yang bekerja secara optimal dalam melaksanakan kegiatannya selama tahun 2017.</p>\n\n<p> Penyusunan LKIP&nbsp; Kecamatan Pameungpeuk ini telah diupayakan sebaik-baiknya, walaupun demikian LKIP Kecamatan Pameungpeuk tidak terlepas dari kekurangan-kekurangan sehubungan dengan kendala-kendala yang dihadapi selama penyusunan LKIP. Namun demikian kami mengupayakan untuk mengatasi kendala tersebut dengan menjalin kerjasama melalui koordinasi dengan pelaksana kegiatan.</p>\n\n<p><a href=\"http://www.bandungkab.go.id/uploads/20180606085115-lkip-2017-kecamatan-pameungpeuk.pdf\"  rgb(0, 0, 0); text-decoration-line: none;\"><img src=\"https://4.bp.blogspot.com/-D2cqy2tvRxY/VMcXnsxvF_I/AAAAAAAAE8c/yQHeGDxeVsA/s1600/download+free+movie+qubool+hai.gif\" width=\"200\" height=\"50\" title=\"LKIP Kecamatan Pameungpeuk Tahun 2017\" alt=\"Kec.Pameungpeuk\" border=\"none\"></a></p>\n\n<p>Sumber : Bidang Program dan Keuangan Kecamatan Pameungpeuk Kab. Bandung</p>', 'atas');
INSERT INTO `dlmbg_menu` VALUES (32, 2, 'Renstra 2016-2021', '', '<p>Dengan mengucap puji syukur kehadirat Allah SWT, atas rahmat dan karunia-Nya,  Rencana Strategis Kecamatan Pameungpeuk Kabupaten Bandung Tahun 2016-2021 dapat diselesaikan.</p>\n\n<p>Penyusunan Renstra Kecamatan Pameungpeuk tersebut berdasarkan dinamika perubahan lingkungan strategis yang demikian cepat, khususnya berkenaan dengan telah ditetapkannyaPeraturan Daerah Nomor 5 tahun 2014 tentang Perubahan Atas Peraturan Daerah Nomor 11 Tahun 2011 tentang Rencana Pembangunan Jangka Menengah Daerah (RPJMD) Kabupaten Bandung Tahun 2016-2021, dengan demikian penyempurnaan dokumen tersebut mutlak diperlukan melalui penajaman pada indikator sasaran, outcome, kegiatan dan output dalam mewujudkan perencanaan pembangunan yang berkualitas dan profesional.</p>\n\n<p>Dalam penyusunan perencanaan strategis, Kecamatan Pameungpeuk mempertimbangkan berbagai permasalahan  strategis yang dihadapi. Penyempurnaan Renstra ini  diharapkan dapat dijadikan sebagai pedoman dan arahan yang tepat bagi Kecamatan Pameungpeuk dalam menyesuaikan strategis organisasi khususnya dalam memenuhi kebutuhan dan harapan <i>stakeholder</i>.</p>\n\n<p><a href=\"http://www.bandungkab.go.id/uploads/20180606085157-renstra-2016-2021-kecamatan-pameungpeuk.pdf\"><img src=\"https://4.bp.blogspot.com/-D2cqy2tvRxY/VMcXnsxvF_I/AAAAAAAAE8c/yQHeGDxeVsA/s1600/download+free+movie+qubool+hai.gif\" width=\"200\" height=\"50\" title=\"Rencana Setrategis Kecamatan Pameungpeuk Tahun 2016-2021\" alt=\"Kec.Pameungpeuk\" border=\"none\"></a></p>\n\n<p>Sumber : Bidang Program dan Keuangan Kecamatan Pameungpeuk Kab. Bandung</p>', 'atas');
INSERT INTO `dlmbg_menu` VALUES (33, 2, 'SK IKU 2017', '', '<p><a href=\"http://www.bandungkab.go.id/uploads/20180607024212-sk-iku-kecamatan-pameungpeuk.pdf\"><img src=\"https://4.bp.blogspot.com/-D2cqy2tvRxY/VMcXnsxvF_I/AAAAAAAAE8c/yQHeGDxeVsA/s1600/download+free+movie+qubool+hai.gif\" width=\"200\" height=\"50\" title=\"SK IKU 2017\" alt=\"Kec.Pameungpeuk\" border=\"none\"  default;\"></a></p>\n\n<p>Sumber : Bidang Program dan Keuangan Kecamatan Pameungpeuk Kab. Bandung</p>', 'atas');
INSERT INTO `dlmbg_menu` VALUES (34, 2, 'Perjanjian Kinerja  2018', '', '<p> <a href=\"http://www.bandungkab.go.id/uploads/20180629082800-perjanjian-kinerja-2018.pdf\"><img src=\"https://4.bp.blogspot.com/-D2cqy2tvRxY/VMcXnsxvF_I/AAAAAAAAE8c/yQHeGDxeVsA/s1600/download+free+movie+qubool+hai.gif\" width=\"200\" height=\"50\" title=\"Perjanjian Kinerja 2018\" alt=\"Kec.Pameungpeuk\" border=\"none\"></a></p>\n\n<p>Sumber : Bidang Program dan Keuangan Kecamatan Pameungpeuk Kab. Bandung</p>', 'atas');
INSERT INTO `dlmbg_menu` VALUES (35, 2, 'PK Aparatur 2018', '', '<p><a href=\"http://www.bandungkab.go.id/uploads/20180607024232-pk-pendayagunaan-aparatur-2018-kecamatan-pameungpeuk.pdf\"><img src=\"https://4.bp.blogspot.com/-D2cqy2tvRxY/VMcXnsxvF_I/AAAAAAAAE8c/yQHeGDxeVsA/s1600/download+free+movie+qubool+hai.gif\" width=\"200\" height=\"50\" title=\"PK Pemberdayaan Aparatur 2018\" alt=\"Kec.Pameungpeuk\" border=\"none\"  default;\"></a></p>\n\n<p>Sumber : Bidang Program dan Keuangan Kecamatan Pameungpeuk Kab. Bandung</p>', 'atas');
INSERT INTO `dlmbg_menu` VALUES (37, 0, 'Status Pelayanan', '/web/search_anjungan', '', 'bawah');
INSERT INTO `dlmbg_menu` VALUES (38, 1, 'test', '', '<p>test</p>', 'atas');

-- ----------------------------
-- Table structure for dlmbg_multi_agenda
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_multi_agenda`;
CREATE TABLE `dlmbg_multi_agenda`  (
  `id_multi_agenda` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `isi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal` int(30) NOT NULL,
  `id_user` int(5) NOT NULL,
  `id_bidang` int(5) NOT NULL,
  `tipe_user` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `stts` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_multi_agenda`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for dlmbg_multi_berita
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_multi_berita`;
CREATE TABLE `dlmbg_multi_berita`  (
  `id_multi_berita` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `isi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal` int(30) NOT NULL,
  `id_user` int(5) NOT NULL,
  `id_bidang` int(5) NOT NULL,
  `tipe_user` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `stts` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `headline` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_multi_berita`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_multi_berita
-- ----------------------------
INSERT INTO `dlmbg_multi_berita` VALUES (61, 'Selamat Datang di Website Kecamatan Margaasih Kabupaten Bandung', 'Selamat Datang di Website Kecamatan Margaasih Kabupaten Bandung\n', '08f0e0486e9871499210dba27278572b.jpg', 1460803075, 1, 0, 'superadmin', '1', 'y');

-- ----------------------------
-- Table structure for dlmbg_multi_pengumuman
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_multi_pengumuman`;
CREATE TABLE `dlmbg_multi_pengumuman`  (
  `id_multi_pengumuman` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `isi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal` int(30) NOT NULL,
  `id_user` int(5) NOT NULL,
  `id_bidang` int(5) NOT NULL,
  `tipe_user` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `stts` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_multi_pengumuman`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_multi_pengumuman
-- ----------------------------
INSERT INTO `dlmbg_multi_pengumuman` VALUES (15, 'Pelayanan Terpadu Sabilulungan    pada tanggal 30 Mei 2018 di Hal. Kec. Margahayu', '<p left;\"=\"\"><img src=\"http://www.bandungkab.go.id/public/uploads/thumbs/size_120_100_20180516094830-fb-img-1525536243652.jpg\" width=\"250px\"></p><p left;\"=\"\"><span initial;\"=\"\">Pelaksanaan Pelayanan Terpadu Sabilulungan akan dilaksanakan di Halaman Kantor Kecamatan Pameungpeuk Kabupaten Bandung pada hari Rabu, 30 Mei 2018. Bagi Masyarakat wilayah Kecamatan Pameungpeuk silahkan dapat datang langsung ke Kantor Kecamatan Pameungpeuk. Pelayanan terpadu Sabilulungan terdiri dari  :</span></p><ol><li>Pembuatan Sim dan Perpanjangan SKCK</li><li>Pelayanan KB</li><li>Pelayanan Pembuatan Akta kelahiran</li><li>Pelayanan Perpustakaan Keliling</li><li>Pembuatan Kartu Kuning</li><li>Pelayanan KTP/ KK</li><li>Pelayanan Sertifikat Tanah</li><li>Konsultasi Label Produk UKM</li><li>Uji Emisi Kendaraan.</li></ol>\n', 1526547000, 2, 0, 'superadmin', '1');

-- ----------------------------
-- Table structure for dlmbg_sekolah_artikel
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_sekolah_artikel`;
CREATE TABLE `dlmbg_sekolah_artikel`  (
  `id_sekolah_artikel` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `isi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal` int(20) NOT NULL,
  `id_admin_sekolah` int(5) NOT NULL,
  `id_sekolah_profil` int(5) NOT NULL,
  `stts` int(1) NOT NULL,
  PRIMARY KEY (`id_sekolah_artikel`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_sekolah_artikel
-- ----------------------------
INSERT INTO `dlmbg_sekolah_artikel` VALUES (6, 'Pelayanan E-KTP', 'L<b><i>OREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUML</i></b>OREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUMLOREM IPSUM', 'fee74facf76ba802c752fc95eabe78e2.jpg', 1525941360, 1, 4, 0);

-- ----------------------------
-- Table structure for dlmbg_sekolah_galeri_sekolah
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_sekolah_galeri_sekolah`;
CREATE TABLE `dlmbg_sekolah_galeri_sekolah`  (
  `id_sekolah_galeri_sekolah` int(5) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `judul` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_admin_sekolah` int(5) NOT NULL,
  `id_sekolah` int(5) NOT NULL,
  `stts` int(1) NOT NULL,
  PRIMARY KEY (`id_sekolah_galeri_sekolah`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for dlmbg_sekolah_guru
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_sekolah_guru`;
CREATE TABLE `dlmbg_sekolah_guru`  (
  `id_sekolah_guru` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jk` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status_pns` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `golongan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tugas` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_sekolah` int(5) NOT NULL,
  `id_jenjang_pendidikan` int(5) NOT NULL,
  `id_kecamatan` int(10) NOT NULL,
  `tempat_lahir` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal_lahir` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal_bertugas` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_sekolah_guru`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_sekolah_guru
-- ----------------------------
INSERT INTO `dlmbg_sekolah_guru` VALUES (2, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (3, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (4, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (5, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (6, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 2, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (7, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 2, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (8, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (9, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (10, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (11, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (12, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 2, 2, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (13, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (14, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (15, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (16, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (17, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (18, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (19, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (20, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (21, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (22, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (23, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (24, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (25, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (26, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (27, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (28, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (29, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (30, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (31, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (32, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (33, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (34, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (35, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (36, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (37, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (38, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (39, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (40, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (41, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (42, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (43, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 3, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (44, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (45, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (46, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (47, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (48, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (49, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (50, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (51, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (52, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (53, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (54, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (55, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (56, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (57, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (58, 'Gede Lumbung', 'L', 'Aktif', 'IV', 'Kepala Sekolah', 4, 1, 1, 'Denpasar', '02/04/1991', '02/13/2011');
INSERT INTO `dlmbg_sekolah_guru` VALUES (60, 'Adi Januardi', 'L', 'Aktif', 'IIIA', 'Guru', 4, 3, 1, 'Denpasar', '04/02/1991', '02/23/2005');

-- ----------------------------
-- Table structure for dlmbg_sekolah_profil
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_sekolah_profil`;
CREATE TABLE `dlmbg_sekolah_profil`  (
  `id_sekolah_profil` int(5) NOT NULL AUTO_INCREMENT,
  `npsn` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_sekolah` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status_sekolah` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_jenjang_pendidikan` int(5) NOT NULL,
  `visi_misi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_kecamatan` int(5) NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `website` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_sekolah_profil`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_sekolah_profil
-- ----------------------------
INSERT INTO `dlmbg_sekolah_profil` VALUES (1, '32.04.09.1004', 'Desa Cilampeni', 'ONLINE', 0, '', 'Bandung', 1, 'info@langonsari.desa.id', 'http://www.sulaeman.desa.id');
INSERT INTO `dlmbg_sekolah_profil` VALUES (2, '	\r\n32.04.09.2003', 'Desa Sangkanhurip', 'ONLINE', 0, '', 'Bandung', 1, 'info@sukasari.desa.id', 'http://www.sukamenak.desa.id');
INSERT INTO `dlmbg_sekolah_profil` VALUES (3, '32.04.09.2002', 'Desa Sukamukti', 'ONLINE', 0, '', 'Bandung', 1, 'info@bojongmanggu.desa.id', 'http://www.margahayutengah.desa.id');
INSERT INTO `dlmbg_sekolah_profil` VALUES (4, '	\r\n32.04.09.2001', 'Desa Gandasari', 'ONLINE', 0, 'Visi :\r\n\r\nMenjadi sekolah berstandar internasional\r\n\r\nMisi :\r\n\r\nMenyiapkan tenaga terampil tingkat menengah bidang teknik industri yang memenuhi standar kompetensi international', 'Bandung', 1, 'info@rancamulya.desa.id', 'http://www.margahayuselatan.desa.id');
INSERT INTO `dlmbg_sekolah_profil` VALUES (5, '32.04.09.2005', 'Desa Pangauban', 'ONLINE', 0, '', 'Bandung', 1, 'info@rancatungku.desa.id', 'http://www.sayati.desa.id');
INSERT INTO `dlmbg_sekolah_profil` VALUES (6, '', 'Desa Katapang', '', 0, '', '', 1, '', '');
INSERT INTO `dlmbg_sekolah_profil` VALUES (7, '', 'Desa Banyusari', '', 0, '', '', 1, '', '');

-- ----------------------------
-- Table structure for dlmbg_sekolah_siswa
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_sekolah_siswa`;
CREATE TABLE `dlmbg_sekolah_siswa`  (
  `id_sekolah_siswa` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nisn` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kelas` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_sekolah` int(5) NOT NULL,
  `id_jenjang_pendidikan` int(5) NOT NULL,
  `id_kecamatan` int(10) NOT NULL,
  PRIMARY KEY (`id_sekolah_siswa`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_sekolah_siswa
-- ----------------------------
INSERT INTO `dlmbg_sekolah_siswa` VALUES (1, 'Alex Firdaus', '1109100350', 'X', 2, 1, 1);
INSERT INTO `dlmbg_sekolah_siswa` VALUES (2, 'Alex Firdaus', '1109100350', 'X', 2, 1, 1);
INSERT INTO `dlmbg_sekolah_siswa` VALUES (3, 'Alex Firdaus', '1109100350', 'X', 1, 1, 1);
INSERT INTO `dlmbg_sekolah_siswa` VALUES (4, 'Alex Firdaus', '1109100350', 'X', 3, 2, 2);
INSERT INTO `dlmbg_sekolah_siswa` VALUES (6, 'Gede Becing Becing', '1109100350', 'X', 4, 3, 1);
INSERT INTO `dlmbg_sekolah_siswa` VALUES (7, 'Annisa Tayara Callista', '1108100200', 'XI', 4, 3, 1);

-- ----------------------------
-- Table structure for dlmbg_setting
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_setting`;
CREATE TABLE `dlmbg_setting`  (
  `id_setting` int(10) NOT NULL AUTO_INCREMENT,
  `tipe` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `title` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `content_setting` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_setting`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_setting
-- ----------------------------
INSERT INTO `dlmbg_setting` VALUES (1, 'site_title', 'Nama Situs', 'Kecamatan Margaasih - Kabupaten Bandung');
INSERT INTO `dlmbg_setting` VALUES (2, 'site_footer', 'Teks Footer', '<h3>Design by <a  target=\"_blank\" href=\"https://www.facebook.com/HansahDarmawan\">HansahDarmawan</a> ©2018</h3>\n<h4>Dikelola Oleh Pemerintah Kecamatan Margaasih dan di dampingi oleh Diskominfo Kab.Bandung</h4>\n<hr>');
INSERT INTO `dlmbg_setting` VALUES (3, 'site_quotes', 'Quotes Situs', 'yes');
INSERT INTO `dlmbg_setting` VALUES (4, 'site_slider', 'Aktifkan Slider', 'yes');
INSERT INTO `dlmbg_setting` VALUES (5, 'site_kepala_dinas', 'CAMAT MARGAASIH', '8ec4c561b448ed8e621e4e7b9323054a.png');
INSERT INTO `dlmbg_setting` VALUES (6, 'site_limit_pengumuman_sidebar', 'Limit View Pengumuman - Sidebar', '5');
INSERT INTO `dlmbg_setting` VALUES (7, 'site_limit_agenda_sidebar', 'Limit View Agenda - Sidebar', '4');
INSERT INTO `dlmbg_setting` VALUES (8, 'site_kepala_dinas_nama_kepala', 'Camat', 'Drs. ASEP RUSWANDI, M.Si');
INSERT INTO `dlmbg_setting` VALUES (9, 'site_kepala_dinas_nip', 'NIP Camat', 'NIP. 19680209 198903 1 003');
INSERT INTO `dlmbg_setting` VALUES (10, 'site_limit_artikel_sekolah_footer', 'Limit View Artikel Sekolah - Footer', '5');
INSERT INTO `dlmbg_setting` VALUES (11, 'site_limit_berita_slider', 'Limit View Berita - Slider', '10');
INSERT INTO `dlmbg_setting` VALUES (12, 'site_slider_always', 'Tampilkan Slider di Semua Halaman', 'yes');
INSERT INTO `dlmbg_setting` VALUES (13, 'site_theme', 'Nama Tampilan', 'hansahdarmawan');
INSERT INTO `dlmbg_setting` VALUES (14, 'site_sambutan', 'Kata Sambutan', '<b><i>Sambutan Camat Kecamatan Margaasih Kabupaten Bandung</i></b>\n<br>\nBerkaitan dengan kebutuhan akan informasi seputar Kegiatan pelayanan yang bersifat primer maupun sekunder diselenggarakan pemerintah sebagai bentuk perwujudan loyalitasnya sebagai abdi masyarakat, kegiatan tersebut dilaksanakan secara efisien dan responsive yaitu pelayanan dilaksanakan melalui pemanfaatan sumberdaya yang seminimal mungkin dengan hasil yang maksimal dan berdasar pada kebutuhan yang diharapkan oleh masyarakat.  Di bidang Pemerintahan tidaklah kalah pentingnya pelayanan itu, bahkan perannya lebih besar karena menyangkut kepentingan umum, bahkan kepentingan rakyat secara keseluruhan. Karena peran pelayanan umum yang diselenggarakan oleh pemerintah melibatkan seluruh aparat Pegawai Negeri makin terasa dengan adanya peningkatan kesadaran bernegara dan bermasyarakat, maka pelayanan telah meningkat kedudukannya di mata masyarakat menjadi suatu hak, yaitu hak atas pelayanan di Kecamatan Margaasih. Namun ternyata hak masyarakat atau perorangan untuk memperoleh pelayanan dari aparat pemerintah terasa belum dapat memenuhi harapan semua pihak, baik masyarakat itu sendiri maupun Pemerintah dan pelayanan umum belum menjadi “budaya” masyarakat.');

-- ----------------------------
-- Table structure for dlmbg_super_album_galeri_dinas
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_super_album_galeri_dinas`;
CREATE TABLE `dlmbg_super_album_galeri_dinas`  (
  `id_abum_galeri_dinas` int(3) NOT NULL AUTO_INCREMENT,
  `nama_album` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_abum_galeri_dinas`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_super_album_galeri_dinas
-- ----------------------------
INSERT INTO `dlmbg_super_album_galeri_dinas` VALUES (24, 'Margahayu Fair -2 2018');
INSERT INTO `dlmbg_super_album_galeri_dinas` VALUES (25, 'Lab. Kecamatan');
INSERT INTO `dlmbg_super_album_galeri_dinas` VALUES (26, 'Kegiatan Kecamatan');
INSERT INTO `dlmbg_super_album_galeri_dinas` VALUES (27, 'Kampung Tematik (1000 Kampung)');

-- ----------------------------
-- Table structure for dlmbg_super_bidang
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_super_bidang`;
CREATE TABLE `dlmbg_super_bidang`  (
  `id_super_bidang` int(5) NOT NULL AUTO_INCREMENT,
  `bidang` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_super_bidang`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_super_bidang
-- ----------------------------
INSERT INTO `dlmbg_super_bidang` VALUES (3, ' Umum & Kepegawaian');
INSERT INTO `dlmbg_super_bidang` VALUES (4, 'Program & Keuangan');
INSERT INTO `dlmbg_super_bidang` VALUES (5, 'Pemerintahan');
INSERT INTO `dlmbg_super_bidang` VALUES (6, 'Ketentraman dan Ketertiban Umum');
INSERT INTO `dlmbg_super_bidang` VALUES (7, 'Pemberdayaan Masyarakat');
INSERT INTO `dlmbg_super_bidang` VALUES (8, 'Sosial Budaya');
INSERT INTO `dlmbg_super_bidang` VALUES (9, 'Pembangunan');

-- ----------------------------
-- Table structure for dlmbg_super_buku_tamu
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_super_buku_tamu`;
CREATE TABLE `dlmbg_super_buku_tamu`  (
  `id_super_buku_tamu` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kontak` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pesan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal` int(20) NOT NULL,
  `stts` int(1) NOT NULL,
  PRIMARY KEY (`id_super_buku_tamu`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_super_buku_tamu
-- ----------------------------
INSERT INTO `dlmbg_super_buku_tamu` VALUES (1, 'Warganet', 'localhost', 'assalamualaikum', 1533385735, 1);
INSERT INTO `dlmbg_super_buku_tamu` VALUES (2, 'asep', 'xx2333', 'uu', 1534170963, 1);

-- ----------------------------
-- Table structure for dlmbg_super_buku_tamu_reply
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_super_buku_tamu_reply`;
CREATE TABLE `dlmbg_super_buku_tamu_reply`  (
  `reply__id` int(11) NOT NULL AUTO_INCREMENT,
  `reply__id_bukutamu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reply__konten` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reply__create_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `reply__update_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `reply__user__create` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reply__user__update` varchar(0) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`reply__id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_super_buku_tamu_reply
-- ----------------------------
INSERT INTO `dlmbg_super_buku_tamu_reply` VALUES (1, '7', 'nuhun,', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1', '');
INSERT INTO `dlmbg_super_buku_tamu_reply` VALUES (2, '2', 'YA', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '6', '');

-- ----------------------------
-- Table structure for dlmbg_super_galeri_dinas
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_super_galeri_dinas`;
CREATE TABLE `dlmbg_super_galeri_dinas`  (
  `id_super_galeri_dinas` int(5) NOT NULL AUTO_INCREMENT,
  `id_user` int(10) NOT NULL,
  `id_album` int(10) NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `judul` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_super_galeri_dinas`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 188 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_super_galeri_dinas
-- ----------------------------
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (168, 5, 24, '4962335c2840b26cd150ad1c66490447.jpg', 'Pak Camat Bersama Kades Margahayu Tengah dan Band ');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (169, 5, 24, '996835d7439637b5e1057d7d97f266de.jpg', 'Pak Camat Bersama Pemenang ');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (170, 5, 24, 'e755a32425556b60884327f65e0369c9.jpg', 'Lomba Mewarnai di Margahayu Fair - 2 2018');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (171, 5, 24, 'c97294ba3e82f08cf0618ab37269e033.jpg', 'Margahayu Fair-2 2018');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (172, 5, 24, '71e068b03b4c25c0554b1a66c7069acc.jpg', 'Margahayu Fair-2 2018');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (173, 5, 24, 'ced6a55de6f3f9dcfbec42ba04fdd567.jpg', 'Margahayu Fair-2 2018');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (174, 5, 24, '6ffc2c8acfb0fd04330e093502550bd8.jpg', 'Margahayu Fair-2 2018');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (175, 5, 24, '70a003c769f25582953111ab6d394533.jpg', 'Margahayu Fair-2 2018');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (176, 5, 24, 'a66c6bd01771c76bbd0f484194621751.jpg', 'Margahayu Fair-2 2018');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (177, 5, 24, '02dc04d49633ae5048a0e390787525e9.jpg', 'Margahayu Fair-2 2018');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (178, 5, 26, '1f2c0428e7b62d8da48f4225a82b4961.jpg', 'Kegiatan Rutin Kecamatan Margahayu');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (179, 5, 26, '2ed157ce3e8c4ef449af01d47863608a.jpg', 'Pelatihan Mamadamkan Api');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (180, 5, 26, 'c7eed3012fcd3d27ad921761012aea37.jpg', 'Pelatihan Mamadamkan Api');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (181, 5, 26, '3fa42ca4d19fbf4c28ff2660a7a6a770.jpg', 'Pemilihan Umum Gubernur Jawa Barat 2018');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (182, 5, 26, '503c2597f40555289b80ad6b57d328c4.jpg', 'Pemilihan Umum Gubernur Jawa Barat 2018');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (183, 5, 27, 'd22a2ba3922f650823aeaf6e678e9424.jpg', 'Kampung Boneka');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (184, 5, 27, 'c15e4bb673e59120663db451242c9f6c.jpg', 'Kampung Sepatu');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (185, 5, 27, '74700a1cfde793a53bd12cb65d4f9a32.jpg', 'Kampung Topi');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (186, 5, 27, 'f933ca13ce4b69f20b3a14473f42dda0.jpg', 'Kampung Militer');
INSERT INTO `dlmbg_super_galeri_dinas` VALUES (187, 5, 27, '503b699ffce42681ed26d9f48c4795d1.jpg', 'Kampung Ransel');

-- ----------------------------
-- Table structure for dlmbg_super_jawaban_poll
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_super_jawaban_poll`;
CREATE TABLE `dlmbg_super_jawaban_poll`  (
  `id_super_jawaban_poll` int(5) NOT NULL AUTO_INCREMENT,
  `id_pertanyaan` int(5) NOT NULL,
  `jawaban` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jum` int(10) NOT NULL,
  PRIMARY KEY (`id_super_jawaban_poll`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_super_jawaban_poll
-- ----------------------------
INSERT INTO `dlmbg_super_jawaban_poll` VALUES (6, 1, 'Sangat Baik', 137);
INSERT INTO `dlmbg_super_jawaban_poll` VALUES (7, 1, 'Baik', 23);
INSERT INTO `dlmbg_super_jawaban_poll` VALUES (8, 1, 'Cukup', 5);
INSERT INTO `dlmbg_super_jawaban_poll` VALUES (9, 1, 'Kurang', 3);
INSERT INTO `dlmbg_super_jawaban_poll` VALUES (10, 1, 'Sangat Kurang', 1);

-- ----------------------------
-- Table structure for dlmbg_super_jenjang_pendidikan
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_super_jenjang_pendidikan`;
CREATE TABLE `dlmbg_super_jenjang_pendidikan`  (
  `id_super_jenjang_pendidikan` int(5) NOT NULL AUTO_INCREMENT,
  `pendidikan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_super_jenjang_pendidikan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_super_jenjang_pendidikan
-- ----------------------------
INSERT INTO `dlmbg_super_jenjang_pendidikan` VALUES (1, 'Strata 1');
INSERT INTO `dlmbg_super_jenjang_pendidikan` VALUES (2, 'SLTA/SMA/SMK/SMEA');
INSERT INTO `dlmbg_super_jenjang_pendidikan` VALUES (3, 'SLTP/SMP');
INSERT INTO `dlmbg_super_jenjang_pendidikan` VALUES (4, 'SD');

-- ----------------------------
-- Table structure for dlmbg_super_kecamatan
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_super_kecamatan`;
CREATE TABLE `dlmbg_super_kecamatan`  (
  `id_super_kecamatan` int(5) NOT NULL AUTO_INCREMENT,
  `kecamatan` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_super_kecamatan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_super_kecamatan
-- ----------------------------
INSERT INTO `dlmbg_super_kecamatan` VALUES (1, 'Katapang');

-- ----------------------------
-- Table structure for dlmbg_super_kepegawaian
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_super_kepegawaian`;
CREATE TABLE `dlmbg_super_kepegawaian`  (
  `id_super_kepegawaian` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nip` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jabatan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_bidang` int(5) NOT NULL,
  `kontak` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_super_kepegawaian`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_super_kepegawaian
-- ----------------------------
INSERT INTO `dlmbg_super_kepegawaian` VALUES (24, 'Heri Purnomo Sidi', '19620904 200801 1 002', 'Administrasi Umum', 9, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (25, 'Dany Feroustiandy', '19790714 201410 1 002', 'Administrasi Trantibum', 6, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (26, 'Yulianti Verisa', '19830311 201412 2 004', 'Administrasi Pemberdayaan Masyarakat', 7, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (27, 'Eka Andriani', '19860525 201412 2 002', 'Pengelola Kepegawaian', 3, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (28, 'E. Wiharsa', '19610510 200702 1 014', 'Administrasi Kependudukan', 5, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (29, 'Nia Susanti, SH', '-', 'Pelaksana PATEN', 3, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (30, 'Chandra Yudiana Efendi, S.IP', '-', 'Pengelola Website', 3, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (31, 'Taufik Suhandi', '-', 'Pengelola Simcan', 4, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (32, 'Dikdik Muchamad Sadikin', '-', 'Operator KTP el', 5, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (33, 'Irfan Gunawan, SE', '-', 'Operator KTP el', 5, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (34, 'Hedi Risyandi, SH', '-', 'Operator SIAK', 5, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (35, 'Diki Mulyadi, A.Md', '-', 'Anggota Satpol PP', 6, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (36, 'Syahrul Solehudin', '-', 'Anggota Satpol PP', 6, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (37, 'Adit Pranustya L, S.IP', '-', 'Anggota Satpol PP', 6, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (38, 'Agus Susela', '-', 'Anggota Satpol PP', 6, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (39, 'Jajang', '-', 'Petugas Kebersihan', 3, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (40, 'Dedi Sugianto', '-', 'Petugas Kebersihan', 3, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (41, 'Ayep Firmansyah', '-', 'Anggota Linmas', 6, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (42, 'Agus Sudrajat', '-', 'Anggota Linmas', 6, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (43, 'Tomi M. Subagja', '-', 'Anggota Linmas', 6, '');
INSERT INTO `dlmbg_super_kepegawaian` VALUES (44, 'Kahya. S', '-', 'Anggota Linmas', 6, '');

-- ----------------------------
-- Table structure for dlmbg_super_link_terkait
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_super_link_terkait`;
CREATE TABLE `dlmbg_super_link_terkait`  (
  `id_super_link_terkait` int(5) NOT NULL AUTO_INCREMENT,
  `nama_link` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `url` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_super_link_terkait`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_super_link_terkait
-- ----------------------------
INSERT INTO `dlmbg_super_link_terkait` VALUES (1, '<img src=\"https://i.imgur.com/p4bsMjA.png\" width=\"100%\">', 'http://www.bandungkab.go.id');
INSERT INTO `dlmbg_super_link_terkait` VALUES (2, '<img src=\"https://i.imgur.com/H8GUmNX.png\" width=\"100%\">', 'http://www.desaku.bandungkab.go.id');
INSERT INTO `dlmbg_super_link_terkait` VALUES (4, '<img src=\"https://i.imgur.com/R2UNIbG.png\" width=\"100%\">', 'https://lapor.go.id/');
INSERT INTO `dlmbg_super_link_terkait` VALUES (5, '<img src=\"https://i.imgur.com/xoBvXJ7.png\" width=\"100%\">', 'https://dpmptsp.bandungkab.go.id/');
INSERT INTO `dlmbg_super_link_terkait` VALUES (6, '<img src=\"https://i.imgur.com/K01M8js.png\" width=\"100%\">', 'http://posyandu.bandungkab.go.id/');

-- ----------------------------
-- Table structure for dlmbg_super_pertanyaan_poll
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_super_pertanyaan_poll`;
CREATE TABLE `dlmbg_super_pertanyaan_poll`  (
  `id_super_pertanyaan_poll` int(5) NOT NULL AUTO_INCREMENT,
  `pertanyaan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `aktif` int(1) NOT NULL,
  PRIMARY KEY (`id_super_pertanyaan_poll`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dlmbg_super_pertanyaan_poll
-- ----------------------------
INSERT INTO `dlmbg_super_pertanyaan_poll` VALUES (1, 'Bagaimana menurut anda tentang website ini?', 1);

-- ----------------------------
-- Table structure for dlmbg_super_statis
-- ----------------------------
DROP TABLE IF EXISTS `dlmbg_super_statis`;
CREATE TABLE `dlmbg_super_statis`  (
  `id_super_statis` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `isi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jenis` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_super_statis`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for temp__jml_penduduk
-- ----------------------------
DROP TABLE IF EXISTS `temp__jml_penduduk`;
CREATE TABLE `temp__jml_penduduk`  (
  `temp__id` int(11) NOT NULL AUTO_INCREMENT,
  `temp__desa_id` int(11) NOT NULL,
  `temp__desa_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `temp__jml_total` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `temp__jml_perempuan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `temp__jml_lakilaki` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `temp__date_created` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `temp__user_created` int(11) NULL DEFAULT NULL,
  `temp__date_updated` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `temp__user_updated` int(11) NULL DEFAULT NULL,
  `temp__jml_kepala_keluarga` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`temp__id`, `temp__desa_nama`, `temp__desa_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of temp__jml_penduduk
-- ----------------------------
INSERT INTO `temp__jml_penduduk` VALUES (1, 0, 'Cigondewah Hilir', '11371', '5460', '5911', '2018-08-25 17:20:10', NULL, '2018-08-25 17:20:10', NULL, '2733');
INSERT INTO `temp__jml_penduduk` VALUES (2, 0, 'Mekar Rahayu', '61974', '30205', '31769', '2018-08-25 17:20:10', NULL, '2018-08-25 17:20:10', NULL, '17988');
INSERT INTO `temp__jml_penduduk` VALUES (3, 0, 'Lagadar', '497', '18', '479', '2018-08-25 17:20:11', NULL, '2018-08-25 17:20:11', NULL, '1');
INSERT INTO `temp__jml_penduduk` VALUES (4, 0, 'Margaasih', '45846', '22193', '23653', NULL, NULL, NULL, NULL, '14371');
INSERT INTO `temp__jml_penduduk` VALUES (5, 0, 'Rahayu', '0', '0', '0', NULL, NULL, NULL, NULL, '0');
INSERT INTO `temp__jml_penduduk` VALUES (6, 0, 'Nanjung', '0', '0', '0', NULL, NULL, NULL, NULL, '0');

SET FOREIGN_KEY_CHECKS = 1;
